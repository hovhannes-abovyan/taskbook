<?php

namespace above\libs;


class Functions
{
    public static function Debugger($arr)
    {
        echo '<pre style="background: #263238;color: #fff;font-size: 16px">' . print_r($arr, true) . '</pre>';
    }

    public static function redirect($http = false)
    {
        if ($http) {
            $redirect = $http;
        } else {
            $redirect = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : PATH;
        }
        header("Location: $redirect");
        exit;
    }

    public static function h($str)
    {
        return htmlspecialchars($str, ENT_QUOTES);
    }
}
