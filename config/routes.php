<?php

use above\Router;

Router::add('^admin/(?P<alias>[a-z0-9-]+)/?$', ['controller' => 'Admin', 'action' => 'update']);
Router::add('^category/(?P<alias>[a-z0-9-]+)/?$', ['controller' => 'Category', 'action' => 'view']);


Router::add('^$', ['controller' => 'Main', 'action' => 'index']);
Router::add('^(?P<controller>[a-z-]+)/?(?P<action>[a-z-]+)?$');
