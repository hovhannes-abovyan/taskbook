<?php

namespace app\controllers;

use above\App;
use R;
use above\libs\Functions;
use above\libs\Pagination;

class MainController extends AppController
{
    public $layout = 'main';

    public function indexAction()
    {
        $this->setMeta('Главная Страница');

        $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
        $perpage = App::$app->getProperty('pagination');
        $total = R::count('tasks');

        $pagination = new Pagination($page, $perpage, $total);
        $start = $pagination->getStart();
        $tasks = R::find('tasks', "LIMIT $start,$perpage");


        $this->set(compact('tasks', 'pagination'));
    }

    public function filterAction()
    {
        $this->setMeta('Главная Страница');

        $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
        $perpage = App::$app->getProperty('pagination');
        $total = R::count('tasks');

        $pagination = new Pagination($page, $perpage, $total);
        $start = $pagination->getStart();
        $filter = $_GET['curr'];
        $tasks = R::find('tasks', "ORDER BY $filter ASC LIMIT $start,$perpage");


        $this->set(compact('tasks', 'pagination'));

    }
}
