<?php

namespace app\controllers;

use above\libs\Functions;
use app\models\Tasks;
use app\models\User;

class UserController extends AppController
{
    public $layout = 'main';

    public function signupAction()
    {
        $this->setMeta('Регистрация');

        if (isset($_SESSION['user']['login'])) {
            header("Location: /");
            exit();
        }
        if (!empty($_POST)) {
            $user = new User();
            $data = $_POST;
            $user->load($data);

            if (!$user->validate($data) || !$user->checkUnique()) {
                $user->getErrors();
                $_SESSION['form_data'] = $data;
            } else {
                $user->attributes['password'] = password_hash($user->attributes['password'], PASSWORD_DEFAULT);
                if ($user->save('user')) {
                    $_SESSION['success'] = "Пользователь зарегистрирован";
                    Functions::redirect();
                } else {
                    $_SESSION['error'] = 'Ошибка!';
                }
            }
            Functions::redirect();
        }
    }

    public function loginAction()
    {
        $this->setMeta('Вход');

        if (isset($_SESSION['user']['login'])) {
            header("Location: /");
            exit();
        }
        if (!empty($_POST)) {
            $user = new User();
            if ($user->login()) {
                header("Location: /");
            } else {
                $_SESSION['error'] = 'Логин/пароль введены неверно';
            }
            Functions::redirect();
        }
    }

    public function newTaskbookAction()
    {
        $this->setMeta('Добавления задач');

        if (!empty($_POST)) {
            $tasks = new Tasks();
            $data = $_POST;

            $tasks->load($data);
            if (!$tasks->validate($data)) {
                $tasks->getErrors();
            } else {
                if ($tasks->saveTasks('tasks')) {
                    $_SESSION['success'] = "Новая задача успешно добавлена";
                    Functions::redirect();
                } else {
                    $_SESSION['error'] = 'Ошибка!';
                }
            }
        }
    }

    public function errAction()
    {

    }

    public function logoutAction()
    {
        if (isset($_SESSION['user'])) unset($_SESSION['user']);
        header("Location: /");
        exit();
    }
}
