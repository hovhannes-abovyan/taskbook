<?php

namespace app\controllers;


use above\libs\Functions;
use app\models\Tasks;

class AdminController extends AppController
{
    public $layout = 'main';

    public function indexAction()
    {
        if (empty($_SESSION['user'])) {
            header('Location: /user/login');
        }

        if ($_SESSION['user']['role'] === 'admin') {
            $tasks = \R::findAll('tasks', '');
            $this->set(compact('tasks'));

        } else {
            header('Location: /user/err');
        }
    }

    public function updateAction()
    {
        $id = $_GET['id'];
        $tasks = \R::findOne('tasks', "WHERE id=?", [$id]);
        $cat = \R::load('tasks', $id);

        if (isset($_POST['submit'])) {
            $coder = $_POST['coder'];
            $cat->status = $coder;
            $end = \R::store($cat);
            if ($end) {
                $page = $_SERVER['PHP_SELF'];
                header("Refresh: url=$page");
                $_SESSION['success'] = "Вы  обновили статус задачи ... ";
                Functions::redirect();
            } else {
                $_SESSION['error'] = 'Ошибка!';
            }
        }

        $this->set(compact('tasks'));

    }
}
