<?php
namespace app\models;


use above\base\Model;
use RedBeanPHP\Plugin\NonStaticBeanHelper;

class Tasks extends Model
{

    public $attributes = [
        'login' => '',
        'email' => '',
        'task_text' => '',
        'status'=>'0'

    ];

    public $rules = [
        'required' => [
            ['login'],
            ['email'],
            ['task_text'],
        ],
        'email' => [
            ['email'],
        ],

    ];

    public function saveTasks($table, $valid = true)
    {
        if ($valid) {
            $tbl = \R::dispense($table);
        } else {
            $tbl = \R::xdispense($table);
        }
        foreach ($this->attributes as $name => $value) {
            $tbl->$name = $value;
        }
        return \R::store($tbl);
    }
}
