<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-md-9 page-content col-thin-left">
                <div class="category-list ">
                    <!-- Sort -->
                    <div class="tab-box ">
                        <ul class="nav nav-tabs add-tabs tablist" role="tablist">
                            <li class="active nav-item">
                                <a href="#" role="tab" data-toggle="tab" class="nav-link">
                                    <span class="badge badge-pill badge-secondary">
                                        <?php echo 'Задач всего ' . count($tasks) ?>
                                    </span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-filter">
                            <!--  Sort -->
                            <select name="sort" title="sort by" id="sorts" class="selectpicker select-sort-by"
                                    data-style="btn-select"
                                    data-width="auto">
                                <option value="login">По: Имени пользователя</option>
                                <option value="email">По: Емаилу</option>
                                <option value="status">По: Статусу</option>
                            </select>
                            <!-- End Sort -->
                        </div>
                    </div>

                    <div class="menu-overly-mask"></div>

                    <div class="tab-content">
                        <div class="tab-pane  active " id="alladslist">
                            <div class="adds-wrapper row no-margin">

                                <?php if ($tasks): ?>
                                    <?php foreach ($tasks as $task): ?>
                                        <div class="item-list">
                                            <div class="row">

                                                <div class="col-sm-7 add-desc-box">
                                                    <div class="ads-details">
                                                        <h5 class="add-title">
                                                            <a href="#">
                                                                <?= $task['task_text'] ?>
                                                            </a>
                                                        </h5>
                                                        <span class="info-row">
                                                    <span class="add-type business-ads tooltipHere"
                                                          data-toggle="tooltip" data-placement="right"
                                                          title="Business Ads">B </span> <span class="date">
                                                        <i class=" icon-clock"> </i> <?= $task['date'] ?> </span> , <span
                                                                    class="category">
                                                                 <i class="fas icon-user-1"></i>
                                                              <span style="color: darkred"><?= $task['login'] ?></span>
                                                            </span>, <span
                                                                    class="item-location">
                                                        <i class="fas icon-mail-1"></i>
                                                                <?= $task['email'] ?>
                                                            </span> </span>
                                                    </div>
                                                </div>

                                                <div class="col-md-3 text-right  price-box">
                                                    <?php if ($task['status'] == '0'): ?>
                                                        <h3 class="item-price" style="color: green;text-align: center">
                                                            Задача выполняется
                                                        </h3>
                                                    <?php else: ?>
                                                        <h3 class="item-price" style="color: red;text-align: center">
                                                            Задача не выполняется
                                                        </h3>
                                                    <?php endif; ?>
                                                </div>

                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>

                </div>

                <div class="pagination-bar text-center">
                    <nav aria-label="Page navigation " class="d-inline-b">
                        <ul class="pagination">
                            <?php if ($pagination->countPages > 1): ?>
                                <?= $pagination ?>
                            <?php endif; ?>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
