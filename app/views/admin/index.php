<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-md-9 page-content">
                <div class="inner-box category-content">
                    <div class="box">
                        <div class="box-body">
                            <h3>Все Задачи</h3>
                            <?php if ($tasks): ?>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Имя пользователя</th>
                                        <th>Email пользователя</th>
                                        <th>Текст задачи</th>
                                        <th>Статус</th>
                                        <th>Поменять статус</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php foreach ($tasks as $task): ?>

                                        <tr class="table-bordered">
                                            <td><?= $task['id']; ?></td>
                                            <td><?= $task['login']; ?></td>
                                            <td><?= $task['email']; ?></td>
                                            <td><?= $task['task_text']; ?></td>
                                            <td>
                                                <?php if ($task['status'] == '0'): ?>
                                                    <span style="color: green">Активен</span>
                                                <?php else:; ?>
                                                    <span style="color: red">Не Активен</span>
                                                <?php endif; ?>
                                            </td>
                                            <td>

                                                <?php if ($task['status'] == '0'): ?>
                                                    <a id="change_status" href="/admin/update?id=<?=$task['id']?>">
                                                        <i class="fa fa-fw fa-adjust" title="activation"></i>
                                                    </a>
                                                <?php else:; ?>
                                                    <a id="change_status" href="/admin/update?id=<?=$task['id']?>">
                                                        <i class="fa fa-fw fa-eraser" style="color: red"></i>
                                                    </a>
                                                <?php endif; ?>

                                            </td>
                                        </tr>

                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



