<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-md-9 page-content">
                <div class="inner-box category-content">
                    <h2 class="title-2 uppercase">
                        <strong>
                            <i class="icon-docs"></i>
                            Добавить новую задачу
                        </strong>
                    </h2>
                    <div class="row">
                        <div class="col-sm-12">
                            <form class="form-horizontal" method="post" action="/admin/update?id=<?= $tasks['id'] ?>"
                                  role="form">
                                <div class="card bg-light card-body mb-3">
                                    <?php if (isset($_SESSION['error'])): ?>
                                        <div class="alert alert-danger">
                                            <?= $_SESSION['error'];
                                            unset($_SESSION['error']) ?>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (isset($_SESSION['success'])): ?>
                                        <div class="alert alert-success">
                                            <?= $_SESSION['success'];
                                            unset($_SESSION['success']) ?>
                                        </div>
                                    <?php endif; ?>
                                    <!-- Select Basic -->
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label" for="seller-area">Статус</label>

                                        <div class="col-sm-8">
                                            <select id="seller-area" name="coder" class="form-control"
                                                    name="status">
                                                <?php if ($tasks['status'] == '0'): ?>
                                                    <option value="0" name="status">
                                                        <?php echo '<span style="color: green">Сейчас Активен</span>' ?>
                                                    </option>
                                                    <option value="1">Деактивировать задачу</option>
                                                <?php endif; ?>
                                                <?php if ($tasks['status'] == '1'): ?>
                                                    <option value="1" name="status">
                                                        <?php echo '<span style="color: green">Сейчас Не  Активен</span>' ?>
                                                    </option>
                                                    <option value="0">Активировать задачу</option>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <input type="submit" name="submit" class="btn btn-success btn-lg"
                                               value="Сохранить задачу">

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

