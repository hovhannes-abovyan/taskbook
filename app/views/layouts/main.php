<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/owl.carousel.css" rel="stylesheet">
    <link href="/css/owl.theme.css" rel="stylesheet">
    <link href="/css/jquery.bxslider.css" rel="stylesheet"/>
    <?= $this->getMeta(); ?>
    <script>
        paceOptions = {
            elements: true
        };
    </script>

    <script src="/js/pace.min.js"></script>
    <script src="/js/modernizr-custom.js"></script>

</head>
<body>


<div id="wrapper">

    <div class="header">
        <nav class="navbar  fixed-top navbar-site navbar-light bg-light navbar-expand-md"
             role="navigation">
            <div class="container">
                <div class="navbar-identity">
                    <a href="/" class="navbar-brand logo logo-title">
        			<span class="logo-icon"><i class="icon icon-search-1 ln-shadow-logo "></i>
        			</span>PHP MVC <span> TEST  </span> </a>
                </div>

                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav ml-auto navbar-right">

                        <li class="dropdown no-arrow nav-item">

                            <?php if (!empty($_SESSION['user'])): ?>
                                <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                                    <span>
                                        Привет <?= htmlspecialchars($_SESSION['user']['login']) ?>
                                    </span>
                                    <i class="icon-user fa"></i>
                                    <i class=" icon-down-open-big fa"></i>
                                </a>

                                <ul class="dropdown-menu user-menu dropdown-menu-right">
                                    <li class="dropdown-item">
                                        <a href="/user/logout"><i class=" icon-logout "></i> Выход</a>
                                    </li>
                                </ul>
                            <?php else: ?>
                            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                                <span> Меню </span>
                                <i class="icon-user fa"></i>
                                <i class=" icon-down-open-big fa"></i>
                            </a>

                            <ul class="dropdown-menu user-menu dropdown-menu-right">

                                <li class="dropdown-item">
                                    <a href="/user/login"><i class="icon-th-thumb"></i> Вход </a>
                                </li>

                                <li class="dropdown-item">
                                    <a href="/user/signup"><i class=" icon-logout "></i> Регистрация</a>
                                </li>

                            </ul>
                        </li>
                        <?php endif; ?>


                        <li class="postadd nav-item">
                            <a href="/user/new-taskbook"
                               class="btn btn-block   btn-border btn-post btn-danger nav-link">
                                Добавить Задачу
                            </a>
                        </li>

                    </ul>
                </div>
            </div>

        </nav>
    </div>

    <?= $content ?>

    <footer class="main-footer">
        <div class="footer-content">
            <div class="container">
                <div class="row">

                    <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                        <div class="footer-col">
                            <h4 class="footer-title">Test PHP MVC </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/js/jquery.min.js">\x3C/script>')</script>
<script src="/js/vendors.min.js"></script>
<script src="/js/validator.js"></script>
<script src="/js/bootstrap.js"></script>
<script src="/js/main.min.js"></script>
<script src="/js/main.js"></script>

</body>
</html>
