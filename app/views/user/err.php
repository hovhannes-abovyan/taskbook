<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-5 login-box">
                <div class="card card-default">
                    <div class="panel-intro text-center">
                        <h2 class="logo-title">
                            <span class="logo-icon">
                                <i class="icon icon-search-1 ln-shadow-logo shape-0"></i>
                            </span> PHP MVC <span>TEST  </span>
                        </h2>

                        <br>

                        <h1 style="color: red">У Вас нет прав вход в Админку</h1>
                </div>
            </div>
        </div>
    </div>
</div>
