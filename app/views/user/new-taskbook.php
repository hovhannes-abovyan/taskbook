<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-md-9 page-content">
                <div class="inner-box category-content">
                    <h2 class="title-2 uppercase">
                        <strong>
                            <i class="icon-docs"></i>
                            Добавить новую задачу
                        </strong>
                    </h2>
                    <div class="row">
                        <div class="col-sm-12">
                            <form class="form-horizontal" method="post" action="/user/new-taskbook" role="form" data-toggle="validator">
                                <div class="card bg-light card-body mb-3">
                                    <?php if (isset($_SESSION['error'])): ?>
                                        <div class="alert alert-danger">
                                            <?= $_SESSION['error'];
                                            unset($_SESSION['error']) ?>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (isset($_SESSION['success'])): ?>
                                        <div class="alert alert-success">
                                            <?= $_SESSION['success'];
                                            unset($_SESSION['success']) ?>
                                        </div>
                                    <?php endif; ?>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label" for="textinput-name">
                                            Имя пользователя
                                        </label>

                                        <div class="col-sm-8 has-feedback">
                                            <input id="textinput-name" name="login"
                                                   placeholder="Введите имя пользователя"
                                                   class="form-control input-md"
                                                   type="text" required>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label" for="seller-email">
                                            Email пользователя
                                        </label>

                                        <div class="col-sm-8 has-feedback">
                                            <input type="text" id="seller-email" name="email" class="form-control"
                                                   placeholder="Введите Email пользователя"
                                                   required>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label" for="seller-Number">
                                            Текст задачи
                                        </label>
                                        <div class="col-sm-8 has-feedback">
                                            <textarea id="seller-Number" name="task_text" class="form-control input-md has-feedback"
                                                      rows="7"
                                                      placeholder="Введите текст для задачи"
                                                      required>

                                            </textarea>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-8">
                                            <button type="submit" id="button1id" class="btn btn-success btn-lg">
                                                Сохранить задачу
                                            </button>
                                        </div>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

