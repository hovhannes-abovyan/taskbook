<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-5 login-box">
                <div class="card card-default">
                    <div class="panel-intro text-center">
                        <h2 class="logo-title">
                            <span class="logo-icon">
                                <i class="icon icon-search-1 ln-shadow-logo shape-0"></i>
                            </span> PHP MVC <span>TEST  </span>
                        </h2>
                        <br>
                        <?php if (isset($_SESSION['error'])): ?>
                            <div class="alert alert-danger">
                                <?= $_SESSION['error'];
                                unset($_SESSION['error']) ?>
                            </div>
                        <?php endif; ?>

                        <?php if (isset($_SESSION['success'])): ?>
                            <div class="alert alert-success">
                                <?= $_SESSION['success'];
                                unset($_SESSION['success']) ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="card-body">
                        <form method="post" action="/user/signup" role="form" data-toggle="validator">
                            <div class="form-group has-feedback">
                                <label for="sender-name" class="control-label">Имя Пользователя: </label>

                                <div class="input-icon"><i class="icon-user fa"></i>
                                    <input id="sender-name" type="text" name="login"
                                           placeholder="Введите Ваше имя пользователя"
                                           class="form-control email" required>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                </div>

                            </div>
                            <div class="form-group has-feedback">
                                <label for="sender-email" class="control-label">Email: </label>

                                <div class="input-icon"><i class="icon-mail-1 fa"></i>
                                    <input id="sender-email" type="text" name="email" placeholder="Введите Ваше емайл"
                                           class="form-control email" required>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                </div>

                            </div>
                            <div class="form-group has-feedback">
                                <label for="user-pass" class="control-label">Пароль:</label>

                                <div class="input-icon"><i class="icon-lock fa"></i>
                                    <input type="password" name="password" class="form-control"
                                           placeholder="Введите Ваше пароль"
                                           id="user-pass"
                                           data-error="Пароль должен включать не менее 6 символов" data-minlength="6"
                                           required>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary  btn-block">Зарегистрировать</button>
                            </div>
                        </form>
                    </div>

                    <div class="card-footer">
                        <div style=" clear:both"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
