<?php

use above\App;

require_once dirname(__DIR__) . '/config/init.php';
require_once CONF. '/routes.php';

new App();
